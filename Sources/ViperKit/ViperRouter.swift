//
//  ViperRouter.swift
//  testapp
//
//  Created by alilyin on 1/20/21.
//

import UIKit
import Foundation

public protocol ViperRouterInput: class {}

open class ViperRouter: NSObject, ViperRouterInput {
    
    public internal (set) weak var viewController: UIViewController?
    
    public required override init() {
        super.init()
    }
    
    // MARK: DEBUG
    
    #if DEBUG
    public func setViewController(_ controller: UIViewController) {
        self.viewController = controller
    }
    #endif /* DEBUG */
}
