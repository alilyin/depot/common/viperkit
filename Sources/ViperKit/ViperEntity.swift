//
//  ViperEntity.swift
//  testapp
//
//  Created by alilyin on 1/20/21.
//

import Foundation

public protocol Entity {}

public final class ViperEntity<T>: Entity {
    
    public let value: T
    
    public required init(_ value: T) {
        self.value = value
    }
}
