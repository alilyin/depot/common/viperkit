//
//  ViperInteractor.swift
//  testapp
//
//  Created by alilyin on 1/20/21.
//

import Foundation

public protocol ViperInteractorInput: class {}
public protocol ViperInteractorOutput: class {}

open class ViperInteractor: NSObject, ViperInteractorInput {
    
    public internal (set) weak var viperPresenter: ViperInteractorOutput?
    
    public func executeAsync(_ operation: @escaping (()->Void)) {
        self.queue.async(execute: operation)
    }
    
    public required override init() {
        super.init()
    }
    
    // MARK: DEBUG
    
    #if DEBUG
    public func setPresenter(_ presenter: ViperInteractorOutput) {
        self.viperPresenter = presenter
    }
    #endif /* DEBUG */
    
    // MARK: Private
    
    private var queue: DispatchQueue = DispatchQueue(label: "viperkit.interactor", qos: .background)
}
