//
//  ViperPresenter.swift
//  testapp
//
//  Created by alilyin on 1/20/21.
//

import Foundation

public protocol ViperParent: class {}

open class ViperPresenter<View: ViperView>: NSObject, ViperParent, ViperViewOutput, ViperInteractorOutput {
    
    public internal (set) var viperInteractor: ViperInteractorInput!
    public internal (set) var viperRouter: ViperRouterInput!
    public internal (set) weak var viperView: View?
    public internal (set) weak var viperParent: ViperParent?
    
    
    open func readyToPlay() {
        assert(viperInteractor != nil)
        assert(viperRouter != nil)
        assert(viperView != nil)
    }
    
    public required init(_ parent: ViperParent) {
        super.init()
        self.viperParent = parent
    }
    
    // MARK: DEBUG
    
    #if DEBUG
    public func setInteractor(_ interactor: ViperInteractorInput) {
        self.viperInteractor = interactor
    }
    
    public func setRouter(_ router: ViperRouterInput) {
        self.viperRouter = router
    }
    
    public func setView(_ view: View) {
        self.viperView = view
    }
    #endif /* DEBUG */
}
