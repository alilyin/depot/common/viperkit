//
//  ViperView.swift
//  testapp
//
//  Created by alilyin on 1/20/21.
//

import UIKit
import Foundation

public protocol ViperViewInput: class {
    func refresh()
}
public protocol ViperViewOutput: class {
    func readyToPlay()
}

open class ViperViewModel {
    public required init() {
    }
}

public protocol ViperView: ViperViewInput {
    associatedtype ViewModel
    var viewModel: ViewModel {get}
}

open class ViperViewController<ViewModel: ViperViewModel>: UIViewController, ViperView {
    
    public internal(set) var viperPresenter: ViperViewOutput!
    
    public var viewModel: ViewModel {
        assert(Thread.current.isMainThread == true)
        return _viewModel
    }
    
    open func refresh() {
        assert(Thread.current.isMainThread == true)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async { [weak self] in
            self?.viperPresenter.readyToPlay()
        }
    }
    
    // MARK: DEBUG
    
    #if DEBUG
    public func setPresenter(_ presenter: ViperViewOutput) {
        self.viperPresenter = presenter
    }
    #endif /* DEBUG */
    
    // MARK: Private
    
    private let _viewModel: ViewModel = ViewModel()
}
