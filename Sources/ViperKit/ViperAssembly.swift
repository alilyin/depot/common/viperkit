//
//  ViperAssembly.swift
//  ViperKit
//
//  Created by alilyin on 1/20/21.
//

import UIKit
import Foundation

public protocol ViperAssembly {
    func assemble(_ dependency: Entity?, parent: ViperParent) -> UIViewController
}

open class ViperModule<ViewModel: ViperViewModel,
                       View: ViperViewController<ViewModel>,
                       Interactor: ViperInteractor,
                       Presenter: ViperPresenter<View>,
                       Router: ViperRouter> {
    
    public static func assemble(_ parent: ViperParent) -> UIViewController {
        let view: View = View()
        let interactor: Interactor = Interactor()
        let presenter: Presenter = Presenter(parent)
        let router: Router = Router()
        view.viperPresenter = presenter
        presenter.viperView = view
        presenter.viperInteractor = interactor
        presenter.viperRouter = router
        interactor.viperPresenter = presenter
        router.viewController = view
        return view
    }
}
