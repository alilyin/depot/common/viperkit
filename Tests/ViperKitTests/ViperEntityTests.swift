import XCTest
@testable import ViperKit

final class ViperEntityTests: XCTestCase {
    
    override func setUpWithError() throws {
    }
    
    override func tearDownWithError() throws {
    }
    
    func testEntity() {
        let entity: ViperEntity<[String]> = ViperEntity(["aaa", "bbb", "ccc"])
        XCTAssert(entity.value.count == 3)
        XCTAssert(entity.value[0] == "aaa")
        XCTAssert(entity.value[1] == "bbb")
        XCTAssert(entity.value[2] == "ccc")
    }
    
    static var allTests = [
        ("testRouter", testEntity),
    ]
}
