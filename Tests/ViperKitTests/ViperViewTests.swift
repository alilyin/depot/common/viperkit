import XCTest
@testable import ViperKit

private class ViewModel: ViperViewModel {
    
}

private class PresenterMock: ViperViewOutput {
    let readyToPlayHandled: XCTestExpectation = XCTestExpectation(description: "readyToPlay() handled")
    
    func readyToPlay() {
        readyToPlayHandled.fulfill()
    }
}

final class ViperViewTests: XCTestCase {
    
    private var presenterMock: PresenterMock!
    private var view: ViperViewController<ViewModel>!
    
    override func setUpWithError() throws {
        self.presenterMock = PresenterMock()
        self.view = ViperViewController<ViewModel>()
        self.view.setPresenter(self.presenterMock)
    }
    
    override func tearDownWithError() throws {
        self.presenterMock = nil
        self.view = nil
    }
    
    func testView() {
        XCTAssert(self.view.viperPresenter === self.presenterMock)
        XCTAssertNoThrow(self.view.viewModel)
    }
    
    func testViewDidLoad() {
        self.view.viewDidLoad()
        self.wait(for: [self.presenterMock.readyToPlayHandled], timeout: 1.0)
    }
    
    func testRefresh() {
        let expectation: XCTestExpectation = XCTestExpectation()
        DispatchQueue.main.async {
            XCTAssertNoThrow(self.view.refresh())
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 1.0)
    }

    static var allTests = [
        ("testView", testView),
        ("testViewDidLoad", testViewDidLoad),
        ("testRefresh", testRefresh),
    ]
}
