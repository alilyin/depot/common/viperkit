import XCTest
@testable import ViperKit

private class ParentMock: ViperParent {
    
}

private class InteractorMock: ViperInteractorInput {
    
}

private class RouterMock: ViperRouterInput {
    
}

private class ViewMock: ViperView {
    typealias ViewModel = ViperViewModel
    
    var viewModel: ViperViewModel = ViperViewModel()
    
    func refresh() {
        
    }
}

final class ViperPresenterTests: XCTestCase {
    
    private var parentMock: ParentMock!
    private var viewMock: ViewMock!
    private var interactorMock: InteractorMock!
    private var routerMock: RouterMock!
    private var presenter: ViperPresenter<ViewMock>!
    
    override func setUpWithError() throws {
        self.viewMock = ViewMock()
        self.interactorMock = InteractorMock()
        self.routerMock = RouterMock()
        self.parentMock = ParentMock()
        self.presenter = ViperPresenter<ViewMock>(self.parentMock)
        
        self.presenter.setView(self.viewMock)
        self.presenter.setInteractor(self.interactorMock)
        self.presenter.setRouter(self.routerMock)
    }
    
    override func tearDownWithError() throws {
        self.parentMock = nil
        self.viewMock = nil
        self.interactorMock = nil
        self.routerMock = nil
        self.presenter = nil
    }
    
    func testPresenter() {
        XCTAssert(self.presenter.viperView === self.viewMock)
        XCTAssert(self.presenter.viperInteractor === self.interactorMock)
        XCTAssert(self.presenter.viperRouter === self.routerMock)
        XCTAssert(self.presenter.viperParent === self.parentMock)
    }
    
    func testReadyToPlay() {
        XCTAssertNoThrow(self.presenter.readyToPlay())
    }

    static var allTests = [
        ("testPresenter", testPresenter),
        ("testReadyToPlay", testReadyToPlay),
    ]
}
