import XCTest
@testable import ViperKit

private class PresenterMock: ViperInteractorOutput {
    
}

final class ViperInteractorTests: XCTestCase {
    
    private var presenterMock: PresenterMock!
    private var interactor: ViperInteractor!
    
    override func setUpWithError() throws {
        self.presenterMock = PresenterMock()
        self.interactor = ViperInteractor()
        self.interactor.setPresenter(self.presenterMock)
    }
    
    override func tearDownWithError() throws {
        self.interactor = nil
        self.presenterMock = nil
    }
    
    func testInteractor() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssert(self.interactor.viperPresenter === self.presenterMock)
    }
    
    func testWeakReferences() {
        XCTAssert(self.interactor.viperPresenter === self.presenterMock)
        self.presenterMock = nil
        XCTAssert(self.interactor.viperPresenter == nil)
    }
    
    func testExecuteAsync() {
        let expectation: XCTestExpectation = XCTestExpectation()
        self.interactor.executeAsync {
            XCTAssert(Thread.current.isMainThread == false, "Interactor operation executed on UI thread")
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 1.0)
    }

    static var allTests = [
        ("testInteractor", testInteractor),
        ("testWeakReferences", testWeakReferences),
        ("testExecuteAsync", testExecuteAsync),
    ]
}
