import XCTest
@testable import ViperKit

final class ViperRouterTests: XCTestCase {
    
    private var viewController: UIViewController!
    private var router: ViperRouter!
    
    override func setUpWithError() throws {
        self.viewController = UIViewController()
        self.router = ViperRouter()
        self.router.setViewController(self.viewController)
    }
    
    override func tearDownWithError() throws {
        self.router = nil
        self.viewController = nil
    }
    
    func testRouter() {
        XCTAssert(self.router.viewController === self.viewController)
    }
    
    func testWeakReferences() {
        XCTAssert(self.router.viewController === self.viewController)
        self.viewController = nil
        XCTAssert(self.router.viewController == nil)
    }

    static var allTests = [
        ("testRouter", testRouter),
        ("testWeakReferences", testWeakReferences),
    ]
}
