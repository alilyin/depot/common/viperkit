import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ViperAssemblyTests.allTests),
        testCase(ViperInteractorTests.allTests),
        testCase(ViperPresenterTests.allTests),
        testCase(ViperEntityTests.allTests),
        testCase(ViperRouterTests.allTests),
        testCase(ViperViewTests.allTests),
    ]
}
#endif
