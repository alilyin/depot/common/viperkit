import XCTest
@testable import ViperKit

private class ViewModel: ViperViewModel {
    
}

private class View: ViperViewController<ViewModel> {
    
}

private class Presenter: ViperPresenter<View> {
    
}

private class Interactor: ViperInteractor {
    
}

private class Router: ViperRouter {
    
}

private class Module: ViperModule<ViewModel, View, Interactor, Presenter, Router> {
    
}

final class ViperAssemblyTests: XCTestCase {
    
    func testAssembly() {
        let controller: UIViewController = Module.assemble(self)
        let view: View = controller as! View
        let presenter: Presenter = view.viperPresenter as! Presenter
        let interactor: Interactor = presenter.viperInteractor as! Interactor
        let router: Router = presenter.viperRouter as! Router
        let parent: ViperParent? = presenter.viperParent
        XCTAssert(presenter.viperView === view)
        XCTAssert(interactor.viperPresenter === presenter)
        XCTAssert(router.viewController === view)
        XCTAssert(parent === self)
    }

    static var allTests = [
        ("testAssembly", testAssembly),
    ]
}

extension ViperAssemblyTests: ViperParent {
    
}
